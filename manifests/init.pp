# manage autofs configuration 

class autofs::client{

	#make proper symlinks
	file{
		"/net":
			ensure => directory;
		"/commonfiles":
			ensure => "/net/commonfiles";
				"/opt":
			ensure => directory;
		"/opt/prog":
			ensure => "/net/prog";
		"/vault":
			ensure => "/net/vault";
	}

############  we should definitly spend more energy here so this is an active list of shows.

	case $operatingsystem {
		darwin: {
			file{
				"/jobs":
					ensure => directory;
				"/jobs/fashion":	
					ensure => "/jobs_automounter/fashion";
				"/jobs/donuts":	
					ensure => "/jobs_automounter/donuts";
				"/jobs/dodge":	
					ensure => "/jobs_automounter/dodge";
				"/jobs/dogstar":	
					ensure => "/jobs_automounter/dogstar";
				"/jobs/surro":	
					ensure => "/jobs_automounter/surro";
				"/jobs/iphone":	
					ensure => "/jobs_automounter/iphone";
				"/jobs/bid":	
					ensure => "/jobs_automounter/bid";
				"/jobs/assembleshow":	
					ensure => "/jobs_automounter/assembleshow";
				"/jobs/rosaiki":	
					ensure => "/jobs_automounter/rosaiki";
				"/jobs/wind":	
					ensure => "/jobs_automounter/wind";
				"/jobs/usforestservice":	
					ensure => "/jobs_automounter/usforestservice";
			}
		}
		default: {
			file{
				"/home":
					ensure => "/net/home";
				"/jobs":
					ensure => "/net/jobs";
			}
		}
	}

	# check if we are in the farmnode chroot	
	case $is_farmnodechroot{
		"true": {
			File {
				owner => root,
				group => root,
				mode => 644,
			}
		}
		'false': {
			
			
			case $operatingsystem {
				darwin: { 
					File {
						owner => root,
						group => wheel,
						mode => 644,
					}
				}
				default: {
				
					File {
						owner => root,
						group => root,
						mode => 644,
						notify => Service[autofs],
					}
				
				}
			}
		}
		default:{ crit("UNKNOWN $is_farmnodechroot value") }
	}

	file { "/etc/auto.master":
		name => $operatingsystem ? {
			darwin => "/etc/auto_master",
			default => "/etc/auto.master",
		},
		source => $operatingsystem ? {
			darwin => "puppet:///autofs/auto_master",
			default => "puppet:///autofs/auto.master",
		},
	}

	file {
		"/etc/auto.cluster":
			name => $operatingsystem ? {
				darwin => "/etc/auto_cluster",
				default => "/etc/auto.cluster",
			},
			source => $operatingsystem ? {
				darwin => "puppet:///autofs/auto_cluster",
				default => $hostname ? {
					piju 	=> "puppet:///autofs/auto.cluster.filecluster.svn",
					ran 	=> "puppet:///autofs/auto.cluster.filecluster.svn",
					default => "puppet:///autofs/auto.cluster.filecluster",
				}
			};
	}

	file { "/etc/auto.home":
		name => $operatingsystem ? {
			darwin => "/etc/auto_home",
			default => "/etc/auto.home",
		},
		source => $operatingsystem ? {
			darwin => "puppet:///autofs/auto_home",
			default => $hostname ? {
				default => "puppet:///autofs/auto.home.filecluster",
			}
		},
	}
	file { "/etc/auto.jobs":
		name => $operatingsystem ? {
			darwin => "/etc/auto_jobs",
			default => "/etc/auto.jobs",
		},
		source => $operatingsystem ? {
			darwin => "puppet:///autofs/auto_jobs",
			default => $hostname ? {
				default => "puppet:///autofs/auto.jobs.filecluster",
			}
		},
	}
	
	include autofs::services
}

class autofs::appserver{

	file { "/etc/auto.master":
		name => "/etc/auto.master",
		source => "puppet:///autofs/auto.master",
	}
	#this will have entries for: 
	#	calendarserver
	# 	ubuntu install server
	# 	packages mount
}



class autofs::services {

	case $operatingsystem {

		darwin: {}
		
		default: {
			package {
					autofs:
						ensure => installed;
					nfs:
						name => $operatingsystem ? {
							debian  => 'nfs-common',
							default => 'nfs',
						};
			}
		}
	}
	file { "/etc/idmapd.conf":
		source => $operatingsystem ? {
			default => "puppet:///autofs/idmapd.conf",
		},
	}

	case $operatingsystem {
		debian: {
			ensure_key_value{ '/etc/default/nfs-common':
				file      => '/etc/default/nfs-common',
				key       => 'NEED_IDMAPD',
				value     => "yes",
				delimiter => '='
			}
		}
	}
	
	
	# gutsynode is the ubuntu node name pre perceus.
	case $is_farmnodechroot{
		'true': {
			# gutsynode needs the services to be setup to start at boot, but not actually running at this time.
			# so we use enable instead of ensure.
			service { 
				
				autofs:
					enable => true,
					pattern => "automount",
					require => Package[autofs];
				
				idmapd:
					name => $operatingsystem ? {
						suse => $lsbdistrelease ? {
							"10.1"  => idmapd,
							"10.3"  => nfs,
							default => idmapd,
						},
						debian => 'nfs-common',
					},	
					enable => true,
					pattern => $operatingsystem ? {
						suse => $lsbdistrelease ? {
							"10.1"  => 'idmapd',
							"10.3"  => 'nfs',
							default => 'idmapd',
						},
						debian => 'rpc.idmapd',
					},	
					require =>[ File["/etc/idmapd.conf"],Package[nfs]],
			}
		}
			
		'false': {
			case $operatingsystem {
				
				darwin: {}
				
				default: {
						service { 
							autofs:
								enable => true,
								ensure => true,
								pattern => "automount",
								require => Package[autofs],
								subscribe => [File["/etc/idmapd.conf"],Package[autofs]];
							
							idmapd:
								name => $operatingsystem ? {
									suse => $lsbdistrelease ? {
										"10.1"  => idmapd,
										"10.3"  => nfs,
										default => idmapd,
									},
									debian => 'nfs-common',
								},	
								enable => true,
								ensure => true,
								pattern => $operatingsystem ? {
									suse => $lsbdistrelease ? {
										"10.1"  => 'idmapd',
										"10.3"  => 'nfs',
										default => 'idmapd',
									},
									debian => 'rpc.idmapd',
								},	
								require =>[ File["/etc/idmapd.conf"],Package[nfs]],
								subscribe => [File["/etc/idmapd.conf"],Package[autofs]]
						}
				}
			}
		}
	}
	
}